<?php // stan April 12, 2009

include '_local.php';   // Инициализация вывода в браузер

include_once 'func_mail.php';   // в конце скрипта, save, send_tar
include_once 'Tar.php';     // Используем формат Tar.Gz

$file = USER_DIR . '/sdb';
// echo $file . "<br />\n";

if ( file_exists( $file ) ) {
  // Создаём архив
  $arc_name = TEMP_PATH . '/sdb.bz2';
  $Tar = new Archive_Tar( $arc_name, 'bz2' );
//   print_ra( $Tar );
  // Добавляем в архив и отправляем
  if ( $Tar->createModify( $file, '', dirname( $file ) ) ) {
    echo "Пытаемся отправить...<br />\n";
    mail_file( 'lishnih@gmail.com', $arc_name, $file );
    echo 'Размер архива: ' . filesize( $Tar->_tarname ) . "<br />\n";
  } else
    echo "Не удалось добавить файл к архиву!<br />\n";
} else
  echo "Файл не найден!<br />\n";

include '_local.php';
?>
