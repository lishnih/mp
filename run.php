#!/usr/bin/php -q
<?php
////////////////////////////////////////////////////////////////
//                                                            //
//       Программирование: Stan                               //
//       Дата создания проекта: 14 июля 2006г.                //
//       Страница автора: http://lishmih.net                  //
//       mailto:lishnih@gmail.com                             //
//                                                            //
//       Скрипт распространяется под лицензией                //
//       GNU GENERAL PUBLIC LICENSE                           //
//       Version 2, June 1991                                 //
//                                                            //
////////////////////////////////////////////////////////////////

include '_local.php';   // Инициализация вывода в браузер
$run_welcome = "MP Версия 0.53 15 февраля 2009г.\n";    // Выводим версию
// Версия ядра пакета: 0.3 1 апреля 2007г.
$run_welcome .= date( 'd.m.Y H:i:s O' )."\n";           // и дату
echo $run_welcome;

// Читаем последовательно сообщения и обрабатываем
if ( $mbox = imap_open( IMAILBOX, ILOGIN, IPASSWORD ) ) {
  $nomsg = imap_num_msg( $mbox );           // Кол-во сообщений
  echo "Всего сообщений: $nomsg\n";
  for ( $j = 1; $j <= $nomsg; $j++ ) {      // Просматриваем все письма в ящике
    $user = '';                 // Сбрасываем пользователя
    ob_start();                 // Буферизуем вывод для каждого пользователя
    echo "======================";          // Выводим данные о сообщении
    $h = mp_header( $mbox, $j );
    print_ra( $h );

    if ( $h['Deleted'] )                    // Если сообщение уже удалено
      echo "Сообщение удалено!\n";
    elseif ( !isset( $h['subject'] ) ) {
      echo "Тема не задана!\n";
      if ( defined( 'SHOW_LEFT_MESSAGES' ) ) {      // Если задано, то выводим текст
        list( $message, $charset ) = retrieve_message( $mbox, $j );
        echo "Кодировка: $charset\n[[[[[[[[[[[[\n";
        $message = mp_explode( $message );
        echo substr( $message, 0, SHOW_LEFT_MESSAGES );
        if ( strlen( $message ) > SHOW_LEFT_MESSAGES )
          echo '..........';
        echo "\n]]]]]]]]]]]]\n";
      }; // if
      if ( defined( 'DEL_ALL' ) )
        imap_delete( $mbox, $j );           // Удаляем сообщение, если требуется
    } else {
      $module = 'app_' . $h['subject'] . '.php';    // Имя модуля
      if ( !file_exists( $module ) ) {              // Если модуля не существует
        echo 'Модуль [' . $h['subject'] . "] не предусмотрен!\n";
        if ( defined( 'SHOW_LEFT_MESSAGES' ) ) {    // Если задано, то выводим текст
          list( $message, $charset ) = retrieve_message( $mbox, $j );
          echo "Кодировка: $charset\n[[[[[[[[[[[[\n";
//        $message = mp_explode( $message );
          $message = trim( html_entity_decode( strip_tags( $message ) ) );
          $message = str_replace( "\r", '', $message );
          echo substr( $message, 0, SHOW_LEFT_MESSAGES );
          if ( strlen( $message ) > SHOW_LEFT_MESSAGES )
            echo '..........';
          echo "\n]]]]]]]]]]]]\n";
        }; // if
        if ( defined( 'DEL_ALL' ) )
          imap_delete( $mbox, $j );         // Удаляем сообщение, если требуется
      } else {
////////////////////////
        list( $message, $charset ) = retrieve_message( $mbox, $j ); // Извлекаем сообщение
        imap_delete( $mbox, $j );           // сразу удаляем письмо на случай, если модуль вызовет ошибки
        echo "Кодировка: $charset\n";
        $message = mp_explode( $message );  // Преобразуем
        $user = $message[1][0];
        $pw = trim( $message[2][0] );
        if ( $user ) {                      // Считываем пользователя
          echo "Пользователь '$user' - ";
          $user_conf = "users/conf_$user.php";    // Настройки пользователя
          if ( file_exists( $user_conf ) ) {// Если настройки существуют
            echo "ok\n";
            if ( !isset( $runs[$user] ) )   // Буферизуем в массив $runs
              $runs[$user] = '';
            include $user_conf;             // загружаем и
            if ( $pw == $pw_user ) {        // проверяем кодовое слово
              $noexecmsg++;                 // Засчитываем письмо и
              $err_code = include $module;  // запускаем необходимый модуль
              if ( $err_code != 1 )
                echo "Модуль возратил ошибку: $err_code\n";
              $runs[$user] .= ob_get_contents();    // сохраняем вывод
            } else
              echo "Пароль задан неверно!\n";
          } else {
            echo "не существует!\n";
            $user = '';
          }; // if
        } else
          echo "Сообщение $j - пустое или не соответствует формату!\n";
////////////////////////
      }; // if
    }; // if
    ob_end_flush();             // Выводим в браузер
  }; // for
  echo "~~~~~~~~~~~~~~~~~~~~~~\n";
  echo "Выполнено сообщений: $noexecmsg";
  if ( defined( 'EXP_REQUIRE' ) )
    imap_close( $mbox, CL_EXPUNGE );
  else
    imap_close( $mbox );
} else
  echo 'Не удалось подключиться к серверу: ' . imap_last_error();

include '_local.php';
?>
