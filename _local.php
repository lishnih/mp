<?php // Stan 10 октября 2006г.
// Этот скрипт вызывается в начале исполняемого скрипта
// для инициализции и в конце - для вывода в броузер
// После второго вызова прекращает выполнение скрипта

// Защита на то, чтобы этот скрипт не вызывался отдельно
//if ( basename( $PHP_SELF ) == basename( __FILE__ ) )
//  exit( 'Hacking attempt!' );

if ( !defined( 'LOCAL_DIR' ) ) {
  define( 'LOCAL_DIR', dirname( __FILE__ ) );
  chdir( LOCAL_DIR );

  include 'conf.php';           // конфигурационный файл
  header( 'Content-Type: text/html; charset=' . $mp_charset );
  ob_start();   // Кешируем вывод в браузер

  require_once 'lib/config.php';
  require_once 'common.php';                    // Отладочная библиотека
  define( 'PHPCOMMON_EXPANDEDSTRINGS', 1 );     // Константа для common.php

  // Если в PHP не включен модуль IMAP, то скрипт не будет работть,
  // но для эмуляции работы можно включать дополнительный скрипт
  // Используется чисто для отладки скрипта
  if ( !function_exists( 'imap_open' ) ) {
    echo "<b>Внимание: Включен режим эмуляции IMAP!</b>\n\n";
    include '_imap_emu.php';
  }; // if
  $noexecmsg = 0;   // Считаем сколько сообщений выполнится
  // Загружаем конфигурационный файл
  include 'func.php';           // основные функции
  //print_r( get_defined_constants() );
} else {
  if ( $imap_alerts = imap_alerts() ) {
    echo "\nСообщения IMAP:\n";
    print_r( $imap_alerts );
  }; // if

  $content = ob_get_contents();
  ob_end_clean();
  $content = "<pre>\n$content\n</pre>\n";

  if ( $noexecmsg ) {
    if ( defined( 'LOG_NAME' ) ) {
      $fp = fopen( LOG_NAME, 'a' );
      fwrite( $fp, "$content\n" );
      fclose( $fp );
    }; // if
    // Весь выведенный на экран текст отправляем по мылу
    mail_message( $content, LOG_TO, 'Отчёт ' . date( 'd.m.Y H:i:s O' ) );
    // Посылаем смс о выполнении скрипта на мобилу
    sms_message( "Кол-во сообщений: $nomsg, обработано: $noexecmsg" );
    // Всем пользоваетелям, кроме админа, рассылаем отчёты
    while( list( $key, $val ) = each( $runs ) ) {
      $pos = strpos( $mail_to[$key], LOG_TO );
      if ( $pos === False )
        mail_message( "<pre>\n$run_welcome$val\n</pre>\n", $mail_to[$key], "$key: " . date( 'd.m.Y H:i:s O' ) );
    }; // while
  }; // if

  //if ( $PHP_SELF )    // Если запуск из веб-сервера, то вывести в броузер
  echo '<html><head><title>mp</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=' . $mp_charset . "\"></head>
<body>\n";
  echo $content;
  echo "\n</body></html>";
  exit();
}; // if
?>
