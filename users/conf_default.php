<?php // Stan 21 октября 2006г.
if ( !defined( 'LOCAL_DIR' ) ) die( 'Hacking attempt' );

// Пароль для этого пользователя
   $pw_user = 'antispam_1234567';

// download   - открыт для всех пользователей
// ==========================================
// Файлы больше следующего размера будут разбиваться на блоки этого размера
// размер определяется после упаковывания, если оно имеет место
   $split_file_size = 3145728;

// imap       - требуется таблица
// ==========================================
// Предопределённые почтовые ящики
   $mboxes[$user][1] = array( 'mailbox'  => '{localhost/notls}',
                              'login'    => 'root',
                              'password' => '' );

// Эти ящики будут подсвечиваться заданым цветом
   $highlight_from[$user][] = array( '',      'debian.org',  'green' );

   $highlight_from[$user][] = array( 'admin', 'lishnih.net', 'green' );

   $highlight_to[$user][]   = array( 'admin', '',            'darkred' );

// mysql      - требуется таблица
// ==========================================
// Предопределённые базы данных
   $mydbserver[$user][1] = array( 'dbhost'   => 'localhost',
                                  'dbuser'   => 'root',
                                  'password' => '' );

// phpbb      - требуется таблица
// ==========================================
// Предопределённые форумы
   // форум lishnih.net
   $myphpbb[$user][1] = array( 'user' => 'Stan',
                               'scriptname' => '/opt/home/php/public_html/localhost/forum/post.php' );

// selfupdate - доступ открыт
// ==========================
   $supdate_access = "$user+$pw_user";

// sh         - доступ открыт
// ==========================
   $shell_access = "$user+$pw_user";

// update     - доступ открыт
// ==========================
   $update_access = "$user+$pw_user";
   $update_dir = USER_DIR;

// Все письма будут отправляться этому адресату
   $mail_to[$user] = 'user@domain';
?>
